import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);



  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final Completer<WebViewController> controller = Completer();

  @override
  Widget build(BuildContext context) {


    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Web view"),
          actions: [
            NavigationControls(webViewControllerFuture: controller.future,),
            SampleMenu(controller: controller.future,)
          ],
        ),
        body: WebView(
          initialUrl: "https://www.facebook.com/",
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController){
            controller.complete(webViewController);
          },
          navigationDelegate: (NavigationRequest request){
            if(request.url.startsWith('https://www.youtube.com')){
              print("Blocking navigation");
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      )
    );
  }
}


enum MenuOptions{
  listCookies,
  clearCookies,
  navigationDelegate
}

class SampleMenu extends StatelessWidget {
  SampleMenu({Key? key, required this.controller}) : super(key: key);

  final Future<WebViewController> controller;
  final CookieManager cookieManager = CookieManager();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
      future: controller,
      builder: (BuildContext context, AsyncSnapshot<WebViewController> snapshot){
        return PopupMenuButton<MenuOptions>(
          itemBuilder: (BuildContext context) => <PopupMenuEntry<MenuOptions>>[
            const PopupMenuItem<MenuOptions>(
              value: MenuOptions.listCookies,
              child: Text('List Cookies'),
            ),
            const PopupMenuItem<MenuOptions>(
              value: MenuOptions.clearCookies,
              child: Text('Clear Cookies'),
            ),
            const PopupMenuItem<MenuOptions>(
              value: MenuOptions.navigationDelegate,
              child: Text('Navigation Delegate'),
            ),
          ],
          onSelected: (MenuOptions value){
            switch(value){
              case MenuOptions.listCookies:
                listCookies(snapshot.data, context);
                break;
              case MenuOptions.clearCookies:
                clearCookies(context);
                break;
              case MenuOptions.navigationDelegate:
                navigationDelegate(snapshot.data, context);
                break;
            }
          },
        );
      }
    );
  }

  // show 2 link
  navigationDelegate(WebViewController? controller, BuildContext context) async {
    const String examplepage = '''
      <!DOCTYPE html><html>
      <head><title>Navigation Delegate Example</title></head>
      <body>
      <p>
      The navigation delegate is set to block navigation to the youtube website.
      </p>
      <ul>
      <ul><a href="https://www.youtube.com/">https://www.youtube.com/</a></ul>
      <ul><a href="https://www.google.com/">https://www.google.com/</a></ul>
      </ul>
      </body>
      </html>
      ''';

    final String contentBase64 = base64Encode(const Utf8Encoder().convert(examplepage));
    controller!.loadUrl('data:text/html;base64,$contentBase64');
  }


  clearCookies(BuildContext context) async {
    final bool hadCookies = await cookieManager.clearCookies();
    String message = 'There were cookies. Now, they are gone!';
    if (!hadCookies) {
      message = 'There are no cookies.';
    }
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(message),)
    );
  }

  listCookies(WebViewController? controller, BuildContext context) async {
    final String cookies = await controller!.evaluateJavascript('document.cookie');
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('Cookies: '),
              Text(cookies),
            ],
          ),
        )
    );
  }
}


class NavigationControls extends StatelessWidget {
  const NavigationControls({required this.webViewControllerFuture});
  final Future<WebViewController> webViewControllerFuture;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: webViewControllerFuture,
      builder: (BuildContext context, AsyncSnapshot<WebViewController> snapshot){
        final bool webViewReady =
            snapshot.connectionState == ConnectionState.done;
        final WebViewController? controller = snapshot.data;
        return Row(
          children: [
            IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: !webViewReady
              ? null
              : () async{
                if(await controller!.canGoBack()){
                  controller.goBack();
                }
                else{
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text("No Back history item"),)
                  );
                }
              },
            ),
            IconButton(
              icon: Icon(Icons.arrow_forward_ios),
              onPressed: !webViewReady
                  ? null
                  : () async{
                if(await controller!.canGoForward()){
                  controller.goForward();
                }
                else{
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text("No Forward history item"),)
                  );
                }
              },
            ),
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: !webViewReady
                  ? null
                  : () async{
                    controller!.reload();
              },
            )
          ],
        );
      },
    );
  }
}
